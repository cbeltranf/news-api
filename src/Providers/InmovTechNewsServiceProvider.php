<?php

namespace Inmovsoftware\NewsApi\Providers;

use Illuminate\Support\ServiceProvider;

use Inmovsoftware\NewsApi\Http\Resources\V1\GlobalCollection;

class InmovTechNewsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(\Illuminate\Routing\Router $router)
    {
        $this->loadRoutesFrom(__DIR__.'/../routes/routes.php');
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

        $this->app->make('Inmovsoftware\NewsApi\Models\V1\News');
        $this->app->make('Inmovsoftware\NewsApi\Models\V1\NewsCategory');
        $this->app->make('Inmovsoftware\NewsApi\Http\Controllers\V1\NewsController');
        $this->app->make('Inmovsoftware\NewsApi\Http\Controllers\V1\NewsCategoryController');
        $this->registerHandler();
    }


    protected function registerHandler()
    {
        \App::singleton(
            Illuminate\Http\Resources\Json\ResourceCollection::class,
            GlobalCollection::class
        );

    }


}
